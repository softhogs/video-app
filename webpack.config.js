var path = require("path");
var webpack = require("webpack");
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
  entry: './app/main.ts',
  output: {
    publicPath: "/www",
    path: __dirname + '/www',
    filename: 'bundle.js'
  },
  resolve: {
    root: [path.join(__dirname, "bower_components")],
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
  },
  plugins: [
    new webpack.ResolverPlugin(
      new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["dependencies", "main"])
    ),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new ExtractTextPlugin('bundle.css', {
      allChunks: true
    }),
    new LiveReloadPlugin({
      appendScriptTag: true
    })
  ],
  module: {
    loaders: [
      { test: /\.scss$/, loader: ExtractTextPlugin.extract('css!sass') },
      { test: /\.css$/, loader: ExtractTextPlugin.extract('css') },
      { test: /\.ts(x?)$/, loader: 'ts-loader' },
      { test: /\.html$/, loader: 'html' }
    ]
  }
};