import 'jquery';

import 'angular';
import 'angular-ui-router';
import 'angular-material';

import VideoService from './services/video';
import SettingsService from './services/settings';

import Router from './router';
import Theme from './theme';

import validateVideoUrl from './filters/validateVideoUrl';
import isEmpty from './filters/isEmpty';
import trusted from './filters/trusted';

import appVideoListItems from './directives/video-list-items';
import appVideoTileItems from './directives/video-tile-items';
import noVideo from './directives/no-video';
import player from './directives/player';
import embedPlayer from './directives/embed-player';
import pagination from './directives/pagination';
import sidebar from './directives/sidebar';

import DefaultController from './controllers/default';

import 'angular-material/angular-material.css';

angular
  .module('video-app', ['ngMaterial', 'ui.router'])
  .service('videoService', VideoService)
  .service('settingsService', SettingsService)
  .filter('validateVideoUrl', validateVideoUrl)
  .filter('isEmpty', isEmpty)
  .filter('trusted', trusted)
  .directive('noVideo', noVideo)
  .directive('player', player)
  .directive('embedPlayer', embedPlayer)
  .directive('pagination', pagination)
  .directive('sidebar', sidebar)
  .directive('appVideoListItems', appVideoListItems)
  .directive('appVideoTileItems', appVideoTileItems)
  .config(Theme)
  .config(Router);
