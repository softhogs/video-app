import DefaultController from "./controllers/default";
import TemplateHelper from './helpers/template';

export default class Router {

  static $inject = ['$stateProvider', '$urlRouterProvider'];

  constructor($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('/', {
        url: '/',
        template: TemplateHelper.load('default'),
        controller: DefaultController,
        controllerAs: 'vm'
      });
  }
};