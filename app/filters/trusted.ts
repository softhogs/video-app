function isTrustedUrl($sce): Function {
  return (url: string): string => $sce.trustAsResourceUrl(url);
}

isTrustedUrl.$inject = ['$sce'];

export default isTrustedUrl;