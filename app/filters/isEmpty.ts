import _ = require('lodash');

export default function isEmpty() {
  return data => _.isEmpty(data);
};