function validateVideoUrl(videoService: app.IVideoService): Function {
  return (url: string): boolean => videoService.validateUrl(url)
}

validateVideoUrl.$inject = ['videoService'];

export default validateVideoUrl;