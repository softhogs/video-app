import TemplateHelper from '../helpers/template';

export default function appVideoTileItems() {
  return {
    restrict: 'E',
    template: TemplateHelper.load('video-tile-items')
  }
}