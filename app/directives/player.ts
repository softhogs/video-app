import TemplateHelper from '../helpers/template';
import PlayerController from '../controllers/player';
function player() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    controller: PlayerController,
    controllerAs: 'video',
    scope: {},
    bindToController: {
       item: '='
    },
    template: TemplateHelper.load('player')
  };
}

export default player;
