import TemplateHelper from '../helpers/template';

export default function embedPlayer() {
  return {
    restrict: 'E',
    scope:{
      width: '@',
      height: '@',
      url: '@'
    },
    template: TemplateHelper.load('embed-player')
  };
}