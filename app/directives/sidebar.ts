import TemplateHelper from '../helpers/template';

export default function pagination() {
  return {
    restrict: 'E',
    template: TemplateHelper.load('sidebar')
  };
}