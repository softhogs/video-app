import TemplateHelper from '../helpers/template';
import PaginationController from '../controllers/pagination';

export default function pagination() {
  return {
    restrict: 'E',
    scope: {},
    bindToController: {
      items: '=',
      pageSize: '=',
      page: '='
    },
    controller: PaginationController,
    controllerAs: 'pagination',
    template: TemplateHelper.load('pagination')
  };
}
