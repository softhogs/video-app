import TemplateHelper from '../helpers/template';

export default function appVideoListItems() {
  return {
    restrict: 'E',
    template: TemplateHelper.load('video-list-items')
  };
}