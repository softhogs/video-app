import TemplateHelper from '../helpers/template';

export default function noVideo() {
  return {
    restrict: 'E',
    template: TemplateHelper.load('no-video')
  };
}