export default class PlayerController {

  static $inject = ['videoService'];
  
  private item: app.IVideoItem;
  private url: string;
  private width: number;
  private height: number;
  private title: string;
  
  constructor(private videoService: app.IMainVideoService) {
    this.url = this.createUrl(this.item);
    this.width = this.width || 480;
    this.height = this.height || 320;
    this.title = this.item.title || '';
  };

  createUrl(item: app.IVideoItem): string {
    return this.videoService.generateEmbeddedURL(item);
  }
}