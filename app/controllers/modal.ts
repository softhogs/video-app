export default class ModalController {

  public url: string = '';

  static $inject = ['$mdDialog', 'videoService'];

  constructor(private $mdDialog, private videoService) {

  }

  add() {
    this.videoService.fetchVideo(this.url);
    this.cancel();
  }

  cancel() {
    this.$mdDialog.cancel();
  }
}