import TemplateHelper from '../helpers/template';
import ModalController from './modal';
import SettingsService from '../services/settings';

import _ = require('lodash');

enum EViews { List, Tiles }

interface IListSettings {
  view: EViews;
  pageSize: number;
  sort: string;
}

export default class DefaultController {

  videos: app.IVideoItem[];
  settings: IListSettings;

  public paginationOptions : number[] = [1, 2, 5, 10, 20, 50, 100];
  public page : number = 0;

  static $inject = ['$mdDialog', 'videoService', 'settingsService', '$mdSidenav'];

  constructor(private $mdDialog, private videoService, private settingsService: SettingsService, private $mdSidenav) {
    this.settings = <IListSettings> settingsService.settings;
    this.videos = videoService.getItems();
  }

  sort() : string {
    return this.settings.sort;
  }

  offset() : number {
    return this.page * this.settings.pageSize;
  }

  toggleButtonLabel() {
    return 'Show ' + (this.showTiles() ? 'list' : 'tiles');
  }

  likeButtonLabel(item) {
    return (item.starred) ? 'Remove from bookmarks' : 'Add to bookmarks';
  }

  toggleView(): void {
    this.settings.view = this.showTiles() ? EViews.List : EViews.Tiles;
  }

  showTiles(): boolean {
    return this.settings.view === EViews.Tiles;
  }

  flush() {
    this.videoService.flush();
  }

  toggleSidebar() {
    this.$mdSidenav('right').toggle();
  }

  toggleLike(item: app.IVideoItem) {
    item.starred = !item.starred;
    this.videoService.sync();
  }

  play(item: app.IVideoItem) {
    this
      .$mdDialog
      .show({
        clickOutsideToClose: true,
        controller: function(item) { this.item = item },
        controllerAs: 'ctrl',
        template: '<player item="ctrl.item"></player>',
        locals: {
          item: item
        }
      });
  }

  remove(item) {
    this.videoService.remove(item);
  }

  showAlert() {
    this.$mdDialog.show({
      controller: ModalController,
      controllerAs: 'ctrl',
      template: TemplateHelper.load('add_video'),
      clickOutsideToClose: false
    });

  }

  order() : string {
    return this.settings.sort;
  }

}
