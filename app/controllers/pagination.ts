export default class PaginationController {

  static $inject = ['settingsService'];
  private page: number;
  private pageSize: number;
  private items: app.IVideoItem[];
  
  constructor(private settingsService) {
  }
  
  hasPrev(): boolean {
    return this.page > 0;
  }
  
  hasNext(): boolean {
    return this.page < (Math.floor(this.items.length / this.settingsService.settings.pageSize) - this.settingsService.settings.pageSize);
  }
  
  prev(): void {
    if (this.hasPrev()) {
      this.page -= 1;
    }
  }
  
  next(): void {
    if (this.hasNext()) {
      this.page += 1;
    }
  }
  
  private currentPage(): number {
    return <number>this.page;
  }

}
