
declare module app {

  interface IVideoService {
    type: any;
    validateUrl(url: string): boolean;
    fetchVideo(ID: string): ng.IPromise<app.IVideoItem>;
    generateEmbeddedURL(item: IVideoItem): string;
  }

  interface IMainVideoService extends app.IVideoService, app.ICollection<app.IVideoItem> {

  }

  interface ICollection<T> {
    getItems(): T[];

    /**
     * Adds document to collection.
     * @param video
     */
    addItem(video: T): void;

    /**
     * Removes all items from collection
     */
    flush(): void;

    /**
     * Removes one item from collection by its reference.
     * @param item
     */
    remove(item: T): void;
  }

  interface IVideoItem {
    videoId: string,
    type: number,
    thumbnail: string,
    duration: string,
    likes: number,
    plays: number,
    title: string,
    creationDate: number,
    starred: boolean
  }

  interface ISettingItem {
    name: string,
    defaults: any
  }

}
