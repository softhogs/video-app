declare function require(path: string): string;

export default class TemplateHelper {

  /**
   * Loads template by name
   * @param name
   * @returns {string} template content
   */
  public static load(name: string): string {
    return require('../templates/' + name + '.html');
  }

}