import _ = require('lodash');

export default class SettingsService {

  private static LOCAL_STORAGE_KEY = 'settings';
  private localStorage: Storage = localStorage;

  public settings = {};
  private realSettings = {};

  private static conf(): app.ISettingItem[] {
    return <app.ISettingItem[]>[
      {
        name: 'pageSize',
        defaults: 5
      },
      {
        name: 'view',
        defaults: 'list'
      },
      {
        name: 'onlyLiked',
        defaults: '',
      },
      {
        name: 'sort',
        defaults: 'creationDate'
      }
    ];
  }

  constructor() {
    this.initRealSettings();

    _.each(SettingsService.conf(), (params) => {
      Object.defineProperty(this.settings, params.name, {
        enumerable: false,
        get: () => this.realSettings[params.name] || params.defaults,
        set: (val) => {
          this.realSettings[params.name] = val;
          this.sync();
        }
      });
    });

  }

  private initRealSettings(): void {
    try {
      this.realSettings = JSON.parse(this.localStorage.getItem(SettingsService.LOCAL_STORAGE_KEY)) || {};
    } catch (e) {
      this.realSettings = {};
    }
  }

  private sync() {
    this.localStorage.setItem(SettingsService.LOCAL_STORAGE_KEY, JSON.stringify(this.realSettings));
  }
}
