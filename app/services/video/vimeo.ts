import VideoServices from './types';

export default class Vimeo implements app.IVideoService {

  public type: VideoServices = VideoServices.Vimeo;

  private static URL_VIDEO: string = 'https://api.vimeo.com/videos/$VID$';
  private static API_KEY: string = 'eb32577ececac419eeecf9e998b3890b';
  private static AUTH_TYPE: string = 'bearer';

  private static vimeoIdRegexp: RegExp = /([0-9]{9})/;
  private static vimeoUrlRegexp: RegExp = /vimeo.*\/(\d+)/i;

  static $inject = ['$http'];

  constructor(private $http: ng.IHttpService, private $q: ng.IQService) {

  }

  /**
   * @returns {string}
   */
  private static getAuthHeader() {
    return Vimeo.AUTH_TYPE + " " + Vimeo.API_KEY;
  }

  /**
   * @param ID
   * @returns {ng.IPromise}
   */
  public fetchVideo(ID: string): ng.IPromise<app.IVideoItem> {
    return this.$http
      .get(Vimeo.createVideoUrl(this.getId(ID)), {
        headers: {
          Authorization: Vimeo.getAuthHeader()
        }
      })
      .then((response): app.IVideoItem => {
        var videoData = response.data;
        return <app.IVideoItem>{
          videoId: this.getId(ID),
          type: this.type,
          thumbnail: videoData['pictures']['sizes'][3]['link'],
          likes: videoData['metadata']['connections']['likes']['total'],
          plays: videoData['stats']['plays'],
          title: videoData['name'],
          duration: videoData['duration'],
          creationDate: Date.now(),
          starred: false
        };
      }, (error): Error=> {
        return new Error('Service problem');
      });
  }

  /**
   * @param ID
   * @returns {string}
   */
  private static createVideoUrl(ID: string): string {
    return Vimeo.URL_VIDEO.replace('$VID$', ID);
  }

  /**
   * @param url
   * @returns {boolean}
   */
  public validateUrl(url: string) {
    return Vimeo.vimeoUrlRegexp.test(url) || Vimeo.vimeoIdRegexp.test(url);
  }

  /**
   * @param url
   * @returns {string}
   */
  public getId(url: string) {
    var ids = Vimeo.vimeoUrlRegexp.exec(url) || Vimeo.vimeoIdRegexp.exec(url);
    return (ids) ? ids[1] : '';
  }

  /**
   * @param item
   * @returns {string}
   */
  generateEmbeddedURL(item: app.IVideoItem): string {
    return `https://player.vimeo.com/video/${item.videoId}?badge=0&autopause=0&player_id=0`;
  }
}
