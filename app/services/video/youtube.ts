import VideoServices from './types';

export default class YouTube implements app.IVideoService {

  public type: VideoServices = VideoServices.YouTube;

  private static URL_VIDEO: string = 'https://www.googleapis.com/youtube/v3/videos?id=$VID$&key=$KEY$&part=snippet,contentDetails,statistics,status';
  private static API_KEY: string = 'AIzaSyB95IZgwpxmGlw9SShGDio_IrRUzF7PlvA';

  private static youTubeUrlRegexp = /(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?/i;
  private static youTubeIdRegexp = /([A-z0-9_]{11})/;

  static $inject = ['$http', '$q'];

  constructor(private $http: ng.IHttpService, private $q: ng.IQService) {

  }

  /**
   * @param ID
   * @returns {ng.IPromise}
   */
  public fetchVideo(ID: string): ng.IPromise<app.IVideoItem> {

    return this.$http
      .get(YouTube.createVideoUrl(this.getId(ID)), {})
      .then((response): app.IVideoItem => {
        var videoData = response.data['items'][0];
        return <app.IVideoItem>{
          videoId: videoData.id,
          type: this.type,
          thumbnail: videoData.snippet.thumbnails.high.url,
          likes: videoData.statistics.likeCount,
          plays: videoData.statistics.viewCount,
          title: videoData.snippet.title,
          duration: videoData.contentDetails.duration,
          creationDate: Date.now(),
          starred: false
        };
      }, (error): Error=> {
        return new Error('Service problem');
      });
  }

  /**
   * @param ID
   * @returns {string}
   */
  private static createVideoUrl(ID: string): string {
    return YouTube.URL_VIDEO.replace('$VID$', ID).replace('$KEY$', YouTube.API_KEY);
  }

  /**
   * @param URL
   * @returns {boolean}
   */
  public validateUrl(url: string) {
    return YouTube.youTubeUrlRegexp.test(url) || YouTube.youTubeIdRegexp.test(url);
  }

  /**
   * @param URL
   * @returns {string}
   */
  public getId(url: string): string {
    var ids = YouTube.youTubeUrlRegexp.exec(url) || YouTube.youTubeIdRegexp.exec(url);
    return (ids) ? ids[1] : '';
  }

  /**
   * @param item
   * @returns {any}
   */
  generateEmbeddedURL(item: app.IVideoItem): string {
    return `https://www.youtube.com/embed/${item.videoId}?autoplay=true`;
  }
}
