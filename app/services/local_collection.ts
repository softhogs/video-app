import 'angular';

export default class LocalCollection<T> implements app.ICollection<T> {
  private storage: Storage = localStorage;

  private items: T[];

  constructor(private key: string) {
    try {
      let items = JSON.parse(this.storage.getItem(this.key));
      this.items = Array.isArray(items) ? items : [];
    } catch (e) {
      this.items = [];
    }
  }

  sync() {
    this
      .storage
      .setItem(this.key, JSON.stringify(this.items));
  }

  getItems(): T[] {
    return this.items;
  }

  addItem(video: T): void {
    this.items.push(video);
    this.sync();
  }

  flush(): void {
    this.items.length = 0;
    this.sync();
  }

  remove(item: T): void {
    var index: number = this.items.indexOf(item);

    if (index !== -1) {
      this.items.splice(index, 1);
    }

    this.sync();
  }

}
