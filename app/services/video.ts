import Vimeo from './video/vimeo';
import YouTube from './video/youtube';
import LocalCollection from './local_collection';

export default class Video implements app.IMainVideoService {

  type: any = null;

  private services: app.IVideoService[];
  public videoCollection: LocalCollection<app.IVideoItem> = new LocalCollection<app.IVideoItem>('videos');

  static $inject = ['$http', '$q'];

  constructor(private $http: ng.IHttpService, private $q: ng.IQService) {
    this.services = [
      new YouTube($http, $q),
      new Vimeo($http, $q)
    ];
  }

  fetchVideo(url: string): ng.IPromise<app.IVideoItem> {
    for (let service of this.services) {
      if (service.validateUrl(url)) {

        return service
          .fetchVideo(url)
          .then((item: app.IVideoItem) => {
            this.videoCollection.addItem(item);
            return item;
          });
      }
    }
  }

  validateUrl(url: string): boolean {
    for (let service of this.services) {
      if (service.validateUrl(url)) {
        return true;
      }
    }

    return false;
  }

  getItems(): app.IVideoItem[] {
    return this.videoCollection.getItems();
  }

  addItem(video: app.IVideoItem): void {
    this.videoCollection.addItem(video);
  }

  flush(): void {
    this.videoCollection.flush();
  }

  remove(item: app.IVideoItem): void {
    this.videoCollection.remove(item);
  }

  sync(): void {
    this.videoCollection.sync();
  }

  /**
   * @param item
   * @returns {string}
   */
  generateEmbeddedURL(item: app.IVideoItem): string {
    for (let s of this.services) {
      if (s.type == item.type) {
        return s.generateEmbeddedURL(item);
      }
    }
    return null;
  }
}