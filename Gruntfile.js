var
  webpackConfig = require("./webpack.config.js"),
  webpack = require("webpack"),
  _ = require('underscore'),
  pkg = require('./package.json');

module.exports = function (grunt) {

  // register all grunt modules
  _.each(pkg.devDependencies, function(_, name) {
    if (/^grunt-.+/i.test(name)) {
      grunt.loadNpmTasks(name);
    }
  });

  grunt.initConfig({
    pkg: pkg,

    // webpack module
    webpack: {
      options: webpackConfig,
      build: {
        plugins: [
          new webpack.optimize.UglifyJsPlugin()
        ]
      },
      "build-dev": {
        progress: false,
        debug: true,
        failOnError: false,
        watch: true,
        keepalive: true,
        inline: true
      }
    },

    "webpack-dev-server": {
      options: {
        webpack: webpackConfig,
        port: 3000
      },
      start: {
        contentBase: webpackConfig.output.path,
        webpack: {
          devtool: "eval",
          debug: true
        }
      }
    },

    clean: {
      www: {
        src: 'www/*'
      }
    },

    hashres: {
      options: {
        encoding: 'utf8',
        fileNameFormat: '${name}-${hash}.${ext}',
        renameFiles: true
      },
      prod: {
        src: [
          'www/*.js',
          'www/*.css'
        ],
        dest: 'www/*.html'
      }
    },

    copy: {
      options: {
        punctuation: ''
      },
      index: {
        src: 'index.html',
        dest: 'www/index.html'
      }
    }

  });

  grunt.registerTask('build', [
    'clean:www',
    'webpack:build',
    'copy:index',
    'hashres:prod'
  ]);

  grunt.registerTask('server', [
    'clean:www',
    'copy:index',
    'webpack-dev-server:start',
    'copy:index',
    'webpack:build-dev'
  ]);

};